package org.dvaganov.organizer.ui.utils

import android.content.res.Resources
import kotlin.math.roundToInt

val Int.dp: Float
    get() = this * Resources.getSystem().displayMetrics.density

val Int.dpSize: Int
    get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()
