package org.dvaganov.organizer.ui.customView

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import org.dvaganov.organizer.ui.utils.dp
import kotlin.math.roundToInt

class WeekView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {

    private val textPaint: Paint = Paint().apply {
        color = Color.BLACK
        textSize = 16.dp
    }

    private val solidLinePaint: Paint = Paint().apply {
        color = Color.BLACK
        style = Paint.Style.STROKE
        strokeWidth = 1.dp
    }
    private val dashedLinePaint: Paint = Paint().apply {
        color = Color.GRAY
        style = Paint.Style.STROKE
        strokeWidth = 1.dp
        pathEffect = DashPathEffect(floatArrayOf(10f, 20f), 0f)
    }

    private var textHeight = 0f

    private var timeColumnWidth = 0f
    private var columnWidth = 0f

    private var solidLines = floatArrayOf()
    private var dashedLines = floatArrayOf()

    private var linesOffset = 0f

    private val startTime = 8
    private val endTime = 22

    private val textPadding = 8.dp

    private var interval: List<Float> = emptyList()

    init {
        updateInterval()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        measureText()

        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val width = MeasureSpec.getSize(widthMeasureSpec)

        var newWidthMeasureSpec = widthMeasureSpec
        columnWidth = width.toFloat() / 3

        if (widthMode == MeasureSpec.UNSPECIFIED) {
            newWidthMeasureSpec = MeasureSpec.makeMeasureSpec(
                2 * timeColumnWidth.roundToInt() + 7 * columnWidth.roundToInt(),
                MeasureSpec.EXACTLY
            )
        }

        super.onMeasure(newWidthMeasureSpec, heightMeasureSpec)

        calculateLineOffset()
        calculateLines()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawLines(solidLines, solidLinePaint)
        canvas.drawLines(dashedLines, dashedLinePaint)

        val topTextPadding = (linesOffset - textHeight) / 2

        interval.forEachIndexed { index, value ->
            val hour = value.toInt()
            val minutes = if (value.minus(hour) == 0f) "00" else "30"

            canvas.drawText(
                "$hour:$minutes",
                textPadding,
                linesOffset * (1 + index) - topTextPadding,
                textPaint
            )
        }
    }

    private measureText() {
        val text = "00:00"
        val bounds = Rect()
        textPaint.getTextBounds(text, 0, text.length, bounds)
        timeColumnWidth = bounds.width() + 2 * textPadding
        textHeight = bounds.height().toFloat()
    }

    private fun updateInterval() {
        interval = mutableListOf<Float>().apply {
            for (time in startTime..endTime) {
                add(time.toFloat())

                // ignore last half hour
                if (time != endTime) {
                    add(time + 0.5f)
                }
            }
        }
    }

    private fun calculateLineOffset() {
        linesOffset = measuredHeight.toFloat() / interval.size
    }

    private fun calculateLines() {
        val width = measuredWidth.toFloat()
        val height = measuredHeight.toFloat()

        val solidLines = mutableListOf<Float>()
        val dashedLines = mutableListOf<Float>()

        fun MutableList<Float>.addHorizontalLine(offset: Float) {
            addAll(listOf(0f, offset, width, offset))
        }

        fun MutableList<Float>.addVerticalLine(offset: Float) {
            addAll(listOf(offset, 0f, offset, height))
        }

        interval.forEachIndexed { index, _ ->
            (if (index % 2 == 0) solidLines else dashedLines)
                .addHorizontalLine((index + 1) * linesOffset)
        }

        solidLines.addVerticalLine(timeColumnWidth.toFloat())
        (1..7).forEach {
            solidLines.addVerticalLine(
                timeColumnWidth.toFloat() + it * columnWidth
            )
        }

        this.solidLines = solidLines.toFloatArray()
        this.dashedLines = dashedLines.toFloatArray()
    }
}
